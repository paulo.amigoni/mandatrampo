import { inject, injectable } from 'tsyringe';

import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import Profession from '../infra/typeorm/entities/Profession';
import IProfessionsRepository from '../repositories/IProfessionsRepository';

interface IRequest {
  name: string;
}
@injectable()
class CreateProfessionService {
  constructor(
    @inject('ProfissionsRepository')
    private professionsRepository: IProfessionsRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute({ name }: IRequest): Promise<Profession> {
    const curri = await this.professionsRepository.create({
      name,
    });

    const cacheKey = `service-list:listaService`;
    await this.cacheProvider.invalidate(cacheKey);

    return curri;
  }
}

export default CreateProfessionService;
