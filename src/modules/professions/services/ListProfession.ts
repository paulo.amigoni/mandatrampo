import { inject, injectable } from 'tsyringe';

import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import IProfessionsRepository from '@modules/professions/repositories/IProfessionsRepository';
import Profession from '@modules/professions/infra/typeorm/entities/Profession';
import { classToClass } from 'class-transformer';

@injectable()
class ListProfession {
  constructor(
    @inject('ProfissionsRepository')
    private professionsRepository: IProfessionsRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute(): Promise<Profession> {
    const cacheKey = `profession-list:listaProfession`;

    const professions = await this.cacheProvider.recover<Profession[]>(
      cacheKey,
    );

    if (!professions) {
      professions = await this.professionsRepository.findAll({});
      await this.cacheProvider.save(cacheKey, classToClass(professions));
    }

    return professions;
  }
}

export default ListProfession;
