import { Request, Response } from 'express';

import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';

import CreateCategorieService from '@modules/categories/services/CreateCategorieService';
import ListCategorie from '@modules/categories/services/ListCategorie';

export default class CategoriesController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listCategorie = container.resolve(ListCategorie);
    const curri = await listCategorie.execute();
    return response.json(classToClass(curri));
  }

  async create(request: Request, response: Response): Promise<Response> {
    const { name } = request.body;
    const createCategorieService = container.resolve(CreateCategorieService);
    const user = await createCategorieService.execute({
      name,
    });

    return response.json(classToClass(user));
  }
}
