import Photos from '../infra/typeorm/entities/Photos';
import ICreatePhotosDTO from '../dtos/ICreatePhotosDTO';

export default interface IPhotosRepository {
  create(data: ICreatePhotosDTO): Promise<Photos>;
  findAll(): Promise<Photos>;
  save(user: Photos): Promise<Photos>;
}
