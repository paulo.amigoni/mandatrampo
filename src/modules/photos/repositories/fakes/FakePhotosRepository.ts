import { uuid } from 'uuidv4';

import IPhotosRepository from '@modules/photos/repositories/IPhotosRepository';
import ICreatePhotosDTO from '@modules/photos/dtos/ICreatePhotosDTO';
import Photos from '@modules/photos/infra/typeorm/entities/Photos';

class FakePhotosRepository implements IPhotosRepository {
  private users: Photos[] = [];

  public async create(userData: ICreatePhotosDTO): Promise<Photos> {
    const user = new Photos();

    Object.assign(user, { id: uuid() }, userData);
    this.users.push(user);
    return user;
  }
}

export default FakePhotosRepository;
