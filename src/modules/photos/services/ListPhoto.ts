import { inject, injectable } from 'tsyringe';

import IPhotosRepository from '@modules/photos/repositories/IPhotosRepository';
import Photos from '@modules/photos/infra/typeorm/entities/Photos';
import Service from '@modules/services/infra/typeorm/entities/Service';

@injectable()
class ListPhoto {
  constructor(
    @inject('PhotosRepository')
    private photosRepository: IPhotosRepository, // @inject('CacheProvider') // private cacheProvider: ICacheProvider,
  ) {}

  public async execute(): Promise<Photos> {
    const photos = await this.photosRepository.findAll();
    return photos;
  }
}

export default ListPhoto;
