import { inject, injectable } from 'tsyringe';

import Photos from '../infra/typeorm/entities/Photos';
import IPhotosRepository from '../repositories/IPhotosRepository';

interface IRequest {
  services_id: string;
  url: string;
}
@injectable()
class CreatePhotoService {
  constructor(
    @inject('PhotosRepository')
    private photosRepository: IPhotosRepository,
  ) {}

  public async execute({ services_id, url }: IRequest): Promise<Photos> {
    const photo = await this.photosRepository.create({
      services_id,
      url,
    });

    return photo;
  }
}

export default CreatePhotoService;
