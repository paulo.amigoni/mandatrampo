import AppError from '@shared/errors/AppError';
import FakePhotosRepository from '../repositories/fakes/FakePhotosRepository';
import CreatePhotoService from './CreatePhotoService';

let fakePhotosRepository: FakePhotosRepository;
let createUser: CreatePhotoService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakePhotosRepository = new FakePhotosRepository();
    createUser = new CreatePhotoService(fakePhotosRepository);
  });

  it('should be able to create a new profession', async () => {
    const user = await createUser.execute({
      url: 'Teste',
    });

    expect(user).toHaveProperty('id');
    expect(user.url).toBe('Teste');
  });
});
