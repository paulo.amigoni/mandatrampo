import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import PhotosController from '../controllers/PhotosController';

const photossRouter = Router();
const photosController = new PhotosController();

photossRouter.get('/', photosController.index);

photossRouter.post(
  '/',
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      service_id: Joi.string().required(),
      url: Joi.string().required(),
    },
  }),
  photosController.create,
);

export default photossRouter;
