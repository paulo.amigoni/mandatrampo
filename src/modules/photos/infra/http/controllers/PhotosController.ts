import { Request, Response } from 'express';

import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';

import CreatePhotoService from '@modules/photos/services/CreatePhotoService';
import ListPhoto from '@modules/photos/services/ListPhoto';

export default class ProfessionsController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listPhoto = container.resolve(ListPhoto);
    const providers = await listPhoto.execute();
    return response.json(classToClass(providers));
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { services_id, url } = request.body;
    const createPhotoService = container.resolve(CreatePhotoService);
    const user = await createPhotoService.execute({
      services_id,
      url,
    });

    return response.json(classToClass(user));
  }
}
