import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import Service from '@modules/services/infra/typeorm/entities/Service';
import uploadConfig from '@config/upload';

import { Exclude, Expose } from 'class-transformer';

@Entity('photoservices')
class Photos {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  services_id: string;

  @Column()
  url: string;

  @ManyToOne(() => Service, service => service.photo)
  @JoinColumn({ name: 'services_id' })
  service: Service;

  @Expose({ name: 'local_url' })
  getAvatarUrl(): string | null {
    if (!this.url) {
      return null;
    }
    switch (uploadConfig.driver) {
      case 'disk':
        return `${process.env.APP_API_URL}/files/${this.url}`;
      case 's3':
        return `https://${uploadConfig.config.aws.bucket}.s3.amazonaws.com/${this.url}`;
      default:
        return null;
    }
  }

  @Exclude()
  @CreateDateColumn()
  created_at: Date;

  @Exclude()
  @UpdateDateColumn()
  updated_at: Date;
}

export default Photos;
