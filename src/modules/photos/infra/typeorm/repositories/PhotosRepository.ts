import { getRepository, Repository } from 'typeorm';

import IPhotosRepository from '@modules/photos/repositories/IPhotosRepository';
import ICreatePhotosDTO from '@modules/photos/dtos/ICreatePhotosDTO';
import Photos from '../entities/Photos';

class PhotosRepository implements IPhotosRepository {
  private ormRepository: Repository<Photos>;

  constructor() {
    this.ormRepository = getRepository(Photos);
  }

  public async findAll(): Promise<any> {
    //  / const photos = await this.ormRepository.find();
    const photos = await this.ormRepository.find();
    return photos;
  }

  public async create(userData: ICreatePhotosDTO): Promise<Photos> {
    const user = this.ormRepository.create(userData);
    await this.ormRepository.save(user);
    return user;
  }
}

export default PhotosRepository;
