import { Router } from 'express';

import multer from 'multer';
import uploadConfig from '@config/upload';

import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import ServiceController from '../controllers/ServiceController';

import ServicePhotoController from '../controllers/ServicePhotoController';
import ServiceDetailController from '../controllers/ServiceDetailController';
import ServiceDetailUserController from '../controllers/ServiceDetailUserController';
import ServiceCategorieController from '../controllers/ServiceCategorieController';

const servicesRouter = Router();
const serviceController = new ServiceController();
const serviceDetailsController = new ServiceDetailController();
const serviceDetailsUserController = new ServiceDetailUserController();
const serviceCategorieController = new ServiceCategorieController();
const servicePhotoController = new ServicePhotoController();

const upload = multer(uploadConfig.multer);

servicesRouter.get('/', serviceController.index);

servicesRouter.get(
  '/:id/details',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  serviceDetailsController.index,
);

servicesRouter.get(
  '/:user_id/user',
  celebrate({
    [Segments.PARAMS]: {
      user_id: Joi.string().uuid().required(),
    },
  }),
  serviceDetailsUserController.index,
);

servicesRouter.get(
  '/:categories_id/categorie',
  celebrate({
    [Segments.PARAMS]: {
      categories_id: Joi.string().uuid().required(),
    },
  }),
  serviceCategorieController.index,
);

servicesRouter.post(
  '/',
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      user_id: Joi.string().uuid().required(),
      curriculum: Joi.string().allow('').optional(),
      name: Joi.string().allow('').optional(),
      description: Joi.string().allow('').optional(),
      address: Joi.string().allow('').optional(),
      city: Joi.string()
        .required()
        .error(new Error('Obrigatório informar a Cidade')),
      phone: Joi.string().allow('').optional(),
      celphone: Joi.string().allow('').optional(),
      email: Joi.string().allow('').optional(),
      site: Joi.string().allow('').optional(),
      link_facebook: Joi.string().allow('').optional(),
      link_instagram: Joi.string().allow('').optional(),
      opening_hours: Joi.string().allow('').optional(),
      categories_id: Joi.string()
        .uuid()
        .required()
        .error(
          new Error(
            'Obrigatório informar uma Categoria, ID deve ser formato UUID',
          ),
        ),
      categories_others: Joi.string().allow('').optional(),
    },
  }),
  serviceController.create,
);

servicesRouter.post(
  '/photo',
  ensureAuthenticated,
  upload.single('url'),
  servicePhotoController.create,
);

export default servicesRouter;
