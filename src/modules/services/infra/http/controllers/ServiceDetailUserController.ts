import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';
import { container } from 'tsyringe';
import ListServiceDetailUser from '@modules/services/services/ListServiceDetailUser';

export default class ServiceDetailUserController {
  public async index(request: Request, response: Response): Promise<Response> {
    const { user_id } = request.params;
    const listServiceDetailUser = container.resolve(ListServiceDetailUser);

    const detail = await listServiceDetailUser.execute({
      user_id,
    });

    return response.json(classToClass(detail));
  }
}
