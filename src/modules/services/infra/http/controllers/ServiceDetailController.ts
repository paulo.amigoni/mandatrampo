import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';
import { container } from 'tsyringe';
import ListServiceDetail from '@modules/services/services/ListServiceDetail';

export default class ServiceDetailController {
  public async index(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const listServiceDetail = container.resolve(ListServiceDetail);

    const detail = await listServiceDetail.execute({
      id,
    });

    return response.json(classToClass(detail));
  }
}
