import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';
import { container } from 'tsyringe';
import ListServiceCategorie from '@modules/services/services/ListServiceCategorie';

export default class ServiceCategorieController {
  public async index(request: Request, response: Response): Promise<Response> {
    const { categories_id } = request.params;
    const listServiceCategorie = container.resolve(ListServiceCategorie);

    const detail = await listServiceCategorie.execute({
      categories_id,
    });

    return response.json(classToClass(detail));
  }
}
