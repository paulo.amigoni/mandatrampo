import { Request, Response } from 'express';
import AppError from '@shared/errors/AppError';
import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';
import CreateServicePhotoService from '@modules/services/services/CreateServicePhotoService';

export default class ServicePhotoController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { services_id } = request.body;
    const { filename: url } = request.file;

    const isAccepted = ['image/png', 'image/jpg', 'image/jpeg'].find(
      formatoAceito => formatoAceito === request.file.mimetype,
    );
    if (!isAccepted) {
      throw new AppError(
        'Tipo de arquivo não aceito, favor enviar .png / .jpg ou .jpeg',
      );
    }

    // const tmpFolder = path.resolve(__dirname, '..', '..', 'tmp');

    // const resizeResult = await sharp(request.file.path)
    //   .resize(128, 128)
    //   .toFile(tmpFolder);
    // console.log('Resize operation terminated: ', resizeResult.);

    const createService = container.resolve(CreateServicePhotoService);

    const service = await createService.execute({
      services_id,
      url,
    });

    return response.json(classToClass(service));
  }

  // public async update(request: Request, response: Response): Promise<Response> {
  //   const { id: user_id } = request.user;
  //   const { filename: avatar_filename } = request.file;

  //   // console.log(request.file);

  //   const updateUserAvatarService = container.resolve(UpdateUserAvatarService);

  //   const user = await updateUserAvatarService.execute({
  //     user_id,
  //     avatar_filename,
  //   });

  //   delete user.password;

  //   return response.json(classToClass(user));
  // }
}
