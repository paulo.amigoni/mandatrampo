import { getRepository, Repository } from 'typeorm';
import Service from '@modules/services/infra/typeorm/entities/Service';

import IServicesRepository from '@modules/services/repositories/IServicesRepository';
import ICreateServiceDTO from '@modules/services/dtos/ICreateServiceDTO';
import IFindAllDetailServiceDTO from '@modules/services/dtos/IFindAllDetailServiceDTO';
import IFindAllDetailUserServiceDTO from '@modules/services/dtos/IFindAllDetailUserServiceDTO';
import IFindAllCategorieDTO from '@modules/services/dtos/IFindAllCategorieDTO';

class ServicesRepository implements IServicesRepository {
  private ormRepository: Repository<Service>;

  constructor() {
    this.ormRepository = getRepository(Service);
  }

  public async findAll(): Promise<Service[] | undefined> {
    const result = await this.ormRepository.find({
      order: { categories_id: 'DESC' },
      relations: ['photo'],
    });

    return result;
  }

  public async findAllIDetailService({
    id,
  }: IFindAllDetailServiceDTO): Promise<Service | undefined> {
    const details = this.ormRepository.findOne({
      where: {
        id,
      },
      relations: ['photo'],
    });
    return details;
  }

  public async findAllIDetailUserService({
    user_id,
  }: IFindAllDetailUserServiceDTO): Promise<Service[] | undefined> {
    const details = this.ormRepository.find({
      where: {
        user_id,
      },
      relations: ['photo'],
    });
    return details;
  }

  public async findAllCategorieService({
    categories_id,
  }: IFindAllCategorieDTO): Promise<Service[] | undefined> {
    const details = this.ormRepository.find({
      where: {
        categories_id,
      },
      relations: ['photo'],
    });
    return details;
  }

  public async create({
    user_id,
    name,
    description,
    address,
    city,
    phone,
    celphone,
    email,
    site,
    link_facebook,
    link_instagram,
    opening_hours,
    categories_id,
    categories_others,
  }: ICreateServiceDTO): Promise<Service> {
    const service = this.ormRepository.create({
      user_id,
      name,
      description,
      address,
      city,
      phone,
      celphone,
      email,
      site,
      link_facebook,
      link_instagram,
      opening_hours,
      categories_id,
      categories_others,
    });

    await this.ormRepository.save(service);

    return service;
  }
}

export default ServicesRepository;
