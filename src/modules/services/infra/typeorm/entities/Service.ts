import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';

import Categorie from '@modules/categories/infra/typeorm/entities/Categorie';
import User from '@modules/users/infra/typeorm/entities/User';
import Photo from '@modules/photos/infra/typeorm/entities/Photos';
import { Exclude } from 'class-transformer';

@Entity('services')
class Service {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  user_id: string;

  @ManyToOne(() => User, { eager: true })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  address: string;

  @Column()
  city: string;

  @Column()
  phone: string;

  @Column()
  celphone: string;

  @Column()
  email: string;

  @Column()
  site: string;

  @Column()
  link_facebook: string;

  @Column()
  link_instagram: string;

  @Column()
  opening_hours: string;

  @Column()
  categories_id: string;

  @ManyToOne(() => Categorie, { eager: true })
  @JoinColumn({ name: 'categories_id' })
  categories: Service;

  @Column()
  categories_others: string;

  @OneToMany(() => Photo, photo => photo.service)
  @JoinColumn({ name: 'id' })
  photo: Photo[];

  @Exclude()
  @CreateDateColumn()
  created_at: Date;

  @Exclude()
  @UpdateDateColumn()
  updated_at: Date;
}

export default Service;
