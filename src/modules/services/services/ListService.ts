import { inject, injectable } from 'tsyringe';

import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import Service from '@modules/services/infra/typeorm/entities/Service';
import IServicesRepository from '@modules/services/repositories/IServicesRepository';
import { classToClass } from 'class-transformer';

interface IRequest {
  user_id: string;
}

@injectable()
class ListService {
  constructor(
    @inject('ServicesRepository')
    private servicesRepository: IServicesRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute(): Promise<Service[] | undefined> {
    const cacheKey = `service-list:listaService`;

    let appointments = await this.cacheProvider.recover<Service[]>(cacheKey);

    if (!appointments) {
      appointments = await this.servicesRepository.findAll({});
      await this.cacheProvider.save(cacheKey, classToClass(appointments));
    }

    return appointments;
  }
}

export default ListService;
