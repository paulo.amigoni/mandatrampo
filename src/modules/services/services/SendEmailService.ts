import { inject, injectable } from 'tsyringe';
import path from 'path';

import AppError from '@shared/errors/AppError';

import IMailProvider from '@shared/container/providers/MailProvider/models/IMailProvider';
import IServicesRepository from '@modules/services/repositories/IServicesRepository';
import IUsersRepository from '@modules/users/repositories/IUsersRepository';

interface IRequest {
  serviceid: string;
}

@injectable()
class SendEmailService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,

    @inject('ServicesRepository')
    private servicesRepository: IServicesRepository,


    @inject('MailProvider')
    private mailProvider: IMailProvider,

  ) {}

  public async execute({ serviceid }: IRequest): Promise<void> {

    const id = serviceid;
    const services = await this.servicesRepository.findAllIDetailService({ id });
    const user = await this.usersRepository.findByEmail(services.email);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const forgotPasswordTemplate = path.resolve(
      __dirname,
      '..',
      'templates',
      'forgot_password.hbs',
    );

    await this.mailProvider.sendEmail({
      to: {
        name: user.name, //trocar pelo do trampo
        email: user.email, //trocar pelo do trampo
      },
      subject: '[MandaTrampo] Cadastro de Serviço',
      templateData: {
        file: forgotPasswordTemplate,
        variables: {
          name: user.name,
          username: services?.user.name,
          description: services?.description,
          service: services?.name,
          address: services?.address,
          city: services?.city,

          link: `/reset`,
          // link: `${process.env.APP_WEB_URL}/reset-password?token=${token}`,
        },
      },
    });


    const forgotPasswordTemplateUser = path.resolve(
      __dirname,
      '..',
      'templates',
      'cadserviceuser.hbs',
    );

    await this.mailProvider.sendEmail({
      to: {
        name: user.name,
        email: user.email,
      },
      subject: '[MandaTrampo] Cadastro de Serviço',
      templateData: {
        file: forgotPasswordTemplateUser,
        variables: {
          name: user.name,
          username: services?.user.name,
          description: services?.description,
          service: services?.name,
          address: services?.address,
          city: services?.city,

          link: `/reset`,
          // link: `${process.env.APP_WEB_URL}/reset-password?token=${token}`,
        },
      },
    });







  }
}

export default SendEmailService;
