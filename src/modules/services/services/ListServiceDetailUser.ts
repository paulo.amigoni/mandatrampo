import { inject, injectable } from 'tsyringe';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import Service from '@modules/services/infra/typeorm/entities/Service';
import IServicesRepository from '../repositories/IServicesRepository';

interface IRequest {
  user_id: string;
}

@injectable()
class ListServiceDetailUser {
  constructor(
    @inject('ServicesRepository')
    private servicesRepository: IServicesRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute({ user_id }: IRequest): Promise<Service[] | undefined> {
    const details = await this.servicesRepository.findAllIDetailUserService({
      user_id,
    });
    return details;
  }
}

export default ListServiceDetailUser;
