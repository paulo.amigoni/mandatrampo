import { inject, injectable } from 'tsyringe';

import Photos from '@modules/photos/infra/typeorm/entities/Photos';
import IPhotosRepository from '@modules/photos/repositories/IPhotosRepository';
import IStorageProvider from '@shared/container/providers/StorageProvider/models/IStorageProvider';

interface IRequest {
  services_id: string;
  url: string;
  file?: string;
}
@injectable()
class CreateServicePhotoService {
  constructor(
    @inject('PhotosRepository')
    private photosRepository: IPhotosRepository,

    @inject('StorageProvider') private storageProvider: IStorageProvider,
  ) {}

  public async execute({ services_id, url }: IRequest): Promise<Photos> {
    const photo = await this.photosRepository.create({
      services_id,
      url,
    });

    const file = await this.storageProvider.saveFile(url);

    return photo;
  }
}

export default CreateServicePhotoService;
