import { inject, injectable } from 'tsyringe';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import Service from '@modules/services/infra/typeorm/entities/Service';
import IServicesRepository from '../repositories/IServicesRepository';

interface IRequest {
  id: string;
}

@injectable()
class ListServiceDetail {
  constructor(
    @inject('ServicesRepository')
    private servicesRepository: IServicesRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute({ id }: IRequest): Promise<Service | undefined> {
    const details = await this.servicesRepository.findAllIDetailService({
      id,
    });
    return details;
  }
}

export default ListServiceDetail;
