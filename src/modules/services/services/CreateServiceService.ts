import { inject, injectable, container } from 'tsyringe';

import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import Service from '../infra/typeorm/entities/Service';
import IServicesRepository from '../repositories/IServicesRepository';
import SendEmailService from '@modules/services/services/SendEmailService';


interface IRequest {
  name: string;
  user_id: string;
  description: string;
  address: string;
  city: string;
  phone: string;
  celphone: string;
  email: string;
  site: string;
  link_facebook: string;
  link_instagram: string;
  opening_hours: string;
  categories_id: string;
  categories_others: string;
}
@injectable()
class CreateServiceService {
  constructor(
    @inject('ServicesRepository')
    private servicesRepository: IServicesRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute({
    user_id,
    name,
    description,
    address,
    city,
    phone,
    celphone,
    email,
    site,
    link_facebook,
    link_instagram,
    opening_hours,
    categories_id,
    categories_others,
  }: IRequest): Promise<Service> {
    const service = await this.servicesRepository.create({
      user_id,
      name,
      description,
      address,
      city,
      phone,
      celphone,
      email,
      site,
      link_facebook,
      link_instagram,
      opening_hours,
      categories_id,
      categories_others,
    });

     const cacheKey = `service-list:listaService`;
     await this.cacheProvider.invalidate(cacheKey);

    //console.log(service.id);
     const serviceid = service.id;
     const sendEmailService = container.resolve(SendEmailService);
     await sendEmailService.execute({ serviceid });

    return service;
  }
}

export default CreateServiceService;
