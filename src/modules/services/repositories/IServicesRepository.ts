import ICreateServiceDTO from '@modules/services/dtos/ICreateServiceDTO';
import IFindAllDetailServiceDTO from '@modules/services/dtos/IFindAllDetailServiceDTO';
import IFindAllDetailUserServiceDTO from '@modules/services/dtos/IFindAllDetailUserServiceDTO';
import IFindAllCategorieDTO from '@modules/services/dtos/IFindAllCategorieDTO';
import Service from '../infra/typeorm/entities/Service';

export default interface IServicesRepository {
  create(data: ICreateServiceDTO): Promise<Service>;
  findAll(data: unknown): Promise<Service[] | undefined>;

  findAllIDetailService(
    data: IFindAllDetailServiceDTO,
  ): Promise<Service | undefined>;

  findAllIDetailUserService(
    data: IFindAllDetailUserServiceDTO,
  ): Promise<Service[] | undefined>;

  findAllCategorieService(
    data: IFindAllCategorieDTO,
  ): Promise<Service[] | undefined>;
}
