import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import CurriculumController from '../controllers/CurriculumController';

const curriculunsRouter = Router();
const curriculumController = new CurriculumController();

curriculunsRouter.get('/', curriculumController.index);

curriculunsRouter.use(ensureAuthenticated);

curriculunsRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      user_id: Joi.string()
        .uuid()
        .required()
        .error(new Error('Obrigatório informar um usuário')),
      curriculum: Joi.string().allow('').optional(),
      link_mediasocial: Joi.string().allow('').optional(),
      description: Joi.string().allow('').optional(),
      profession_id: Joi.string()
        .required()
        .error(new Error('Obrigatório informar uma profissão válida')),
      profession_others: Joi.string().allow('').optional(),
      experience_time: Joi.string().allow('').optional(),
    },
  }),
  curriculumController.create,
);

export default curriculunsRouter;
