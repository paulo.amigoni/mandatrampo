import { inject, injectable } from 'tsyringe';

import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import ICurriculunsRepository from '@modules/curriculuns/repositories/ICurriculunsRepository';
import User from '@modules/users/infra/typeorm/entities/User';

interface IRequest {
  user_id: string;
}

@injectable()
class ListCurriculum {
  constructor(
    @inject('CurriculunsRepository')
    private curriculuinsRepository: ICurriculunsRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ) {}

  public async execute(): Promise<User> {
    const curri = await this.curriculuinsRepository.findAll();
    return curri;
  }
}

export default ListCurriculum;
