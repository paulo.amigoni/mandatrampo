import { inject, injectable } from 'tsyringe';

import IProfessionsRepository from '@modules/professions/repositories/IProfessionsRepository';
import User from '@modules/users/infra/typeorm/entities/User';

@injectable()
class ListService {
  constructor(
    @inject('ProfissionsRepository')
    private professionsRepository: IProfessionsRepository, // @inject('CacheProvider') // private cacheProvider: ICacheProvider,
  ) {}

  public async execute(): Promise<User> {
    const users = await this.professionsRepository.findAll();
    return users;
  }
}

export default ListService;
