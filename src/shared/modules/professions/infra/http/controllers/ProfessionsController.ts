import { Request, Response } from 'express';

import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';

import CreateProfessionService from '@modules/professions/services/CreateProfessionService';
import ListProfession from '@modules/professions/services/ListProfession';

export default class ProfessionsController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listProfession = container.resolve(ListProfession);
    const providers = await listProfession.execute();
    return response.json(classToClass(providers));
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { name } = request.body;

    const createProfessionService = container.resolve(CreateProfessionService);

    const user = await createProfessionService.execute({
      name,
    });

    return response.json(classToClass(user));
  }
}
