import { inject, injectable } from 'tsyringe';
import nodemailer, { Transporter } from 'nodemailer';
import IMailProvider from '../models/IMailProvider';
import ISendMailDTO from '../dtos/ISendMailDTO';
import IMailTemplateProvider from '../../MailTemplateProvider/models/IMailTemplateProvider';

@injectable()
class EtherealMailProdiver implements IMailProvider {
  private client: Transporter;

  constructor(
    @inject('MailTemplateProvider')
    private mailTemplateProvider: IMailTemplateProvider,
  ) {
    // nodemailer.createTestAccount().then(account => {
    //   const transporter = nodemailer.createTransport({
    //     // host: account.smtp.host,
    //     // port: account.smtp.port,
    //     // secure: account.smtp.secure,
    //     // auth: {
    //     //   user: account.user,
    //     //   pass: account.pass,
    //     // },
    //   });



    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: 'aileen.denesik@ethereal.email',
        pass: 'ADbrYCNYmUwmPKkFmu',
      },

      // host: 'smtp.zoho.com',
      // port: 587,
      // auth: {
      //   user: 'contato@mandatrampo.com.br',
      //   pass: 'F@tec2020',
      // },
    });
    this.client = transporter;
    //  });
  }

  public async sendEmail({
    to,
    subject,
    from,
    templateData,
  }: ISendMailDTO): Promise<void> {
    const message = await this.client.sendMail({
      from: {
        name: from?.name || 'Equipe MandaTrampo',
        address: from?.email || 'contato@mandatrampo.com.br',
      },
      to: {
        name: to.name,
        address: to.email,
      },
      subject,
      html: await this.mailTemplateProvider.parse(templateData),
    });

    console.log('Mensagem enviada: %s', message.messageId);
    console.log('Visualizar URL: %s', nodemailer.getTestMessageUrl(message));
  }
}

export default EtherealMailProdiver;
