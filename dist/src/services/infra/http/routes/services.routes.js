"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var celebrate_1 = require("celebrate");
var ensureAuthenticated_1 = __importDefault(require("@modules/users/infra/http/middlewares/ensureAuthenticated"));
var ServiceController_1 = __importDefault(require("../controllers/ServiceController"));
var servicesRouter = express_1.Router();
var serviceController = new ServiceController_1.default();
servicesRouter.use(ensureAuthenticated_1.default);
servicesRouter.post('/', celebrate_1.celebrate((_a = {},
    _a[celebrate_1.Segments.BODY] = {
        user_id: celebrate_1.Joi.string().uuid().required(),
        curriculum: celebrate_1.Joi.string(),
        name: celebrate_1.Joi.string(),
        description: celebrate_1.Joi.string(),
        address: celebrate_1.Joi.string(),
        city: celebrate_1.Joi.string(),
        phone: celebrate_1.Joi.string(),
        celphone: celebrate_1.Joi.string(),
        email: celebrate_1.Joi.string(),
        site: celebrate_1.Joi.string(),
        link_facebook: celebrate_1.Joi.string(),
        link_instagram: celebrate_1.Joi.string(),
        opening_hours: celebrate_1.Joi.string(),
        categories_id: celebrate_1.Joi.string().uuid(),
        categories_others: celebrate_1.Joi.string(),
    },
    _a)), serviceController.create);
exports.default = servicesRouter;
