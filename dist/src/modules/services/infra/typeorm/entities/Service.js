"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Profession_1 = __importDefault(require("@modules/professions/infra/typeorm/entities/Profession"));
var User_1 = __importDefault(require("@modules/users/infra/typeorm/entities/User"));
var Service = /** @class */ (function () {
    function Service() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], Service.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "user_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return User_1.default; }),
        typeorm_1.JoinColumn({ name: 'user_id' }),
        __metadata("design:type", User_1.default)
    ], Service.prototype, "user", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "address", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "city", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "phone", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "celphone", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "site", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "link_facebook", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "link_instagram", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "opening_hours", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "categories_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Profession_1.default; }),
        typeorm_1.JoinColumn({ name: 'categories_id' }),
        __metadata("design:type", Profession_1.default)
    ], Service.prototype, "categories", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Service.prototype, "categories_others", void 0);
    __decorate([
        typeorm_1.CreateDateColumn(),
        __metadata("design:type", Date)
    ], Service.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.UpdateDateColumn(),
        __metadata("design:type", Date)
    ], Service.prototype, "updated_at", void 0);
    Service = __decorate([
        typeorm_1.Entity('services')
    ], Service);
    return Service;
}());
exports.default = Service;
