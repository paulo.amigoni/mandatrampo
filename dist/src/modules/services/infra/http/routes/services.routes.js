"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var celebrate_1 = require("celebrate");
var ensureAuthenticated_1 = __importDefault(require("@modules/users/infra/http/middlewares/ensureAuthenticated"));
var ServiceController_1 = __importDefault(require("../controllers/ServiceController"));
var servicesRouter = express_1.Router();
var serviceController = new ServiceController_1.default();
servicesRouter.use(ensureAuthenticated_1.default);
servicesRouter.post('/', ensureAuthenticated_1.default, celebrate_1.celebrate((_a = {},
    _a[celebrate_1.Segments.BODY] = {
        user_id: celebrate_1.Joi.string().uuid().required(),
        curriculum: celebrate_1.Joi.string().allow('').optional(),
        name: celebrate_1.Joi.string().allow('').optional(),
        description: celebrate_1.Joi.string().allow('').optional(),
        address: celebrate_1.Joi.string().allow('').optional(),
        city: celebrate_1.Joi.string()
            .required()
            .error(new Error('Obrigatório informar a Cidade')),
        phone: celebrate_1.Joi.string().allow('').optional(),
        celphone: celebrate_1.Joi.string().allow('').optional(),
        email: celebrate_1.Joi.string().allow('').optional(),
        site: celebrate_1.Joi.string().allow('').optional(),
        link_facebook: celebrate_1.Joi.string().allow('').optional(),
        link_instagram: celebrate_1.Joi.string().allow('').optional(),
        opening_hours: celebrate_1.Joi.string().allow('').optional(),
        categories_id: celebrate_1.Joi.string()
            .uuid()
            .required()
            .error(new Error('Obrigatório informar uma Categoria, ID deve ser formato UUID')),
        categories_others: celebrate_1.Joi.string().allow('').optional(),
    },
    _a)), serviceController.create);
exports.default = servicesRouter;
