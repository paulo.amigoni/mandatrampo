"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var celebrate_1 = require("celebrate");
var ensureAuthenticated_1 = __importDefault(require("@modules/users/infra/http/middlewares/ensureAuthenticated"));
var CategoriesController_1 = __importDefault(require("../controllers/CategoriesController"));
var categoriesRouter = express_1.Router();
var categoriesController = new CategoriesController_1.default();
categoriesRouter.post('/', ensureAuthenticated_1.default, celebrate_1.celebrate((_a = {},
    _a[celebrate_1.Segments.BODY] = {
        name: celebrate_1.Joi.string().required(),
    },
    _a)), categoriesController.create);
exports.default = categoriesRouter;
