"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Profession_1 = __importDefault(require("@modules/professions/infra/typeorm/entities/Profession"));
var User_1 = __importDefault(require("@modules/users/infra/typeorm/entities/User"));
var Curriculum = /** @class */ (function () {
    function Curriculum() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", String)
    ], Curriculum.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "user_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return User_1.default; }),
        typeorm_1.JoinColumn({ name: 'user_id' }),
        __metadata("design:type", User_1.default)
    ], Curriculum.prototype, "user", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "curriculum", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "link_mediasocial", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "profession_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Profession_1.default; }),
        typeorm_1.JoinColumn({ name: 'profession_id' }),
        __metadata("design:type", Profession_1.default)
    ], Curriculum.prototype, "profession", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "profession_others", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Curriculum.prototype, "experience_time", void 0);
    __decorate([
        typeorm_1.CreateDateColumn(),
        __metadata("design:type", Date)
    ], Curriculum.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.UpdateDateColumn(),
        __metadata("design:type", Date)
    ], Curriculum.prototype, "updated_at", void 0);
    Curriculum = __decorate([
        typeorm_1.Entity('curriculum')
    ], Curriculum);
    return Curriculum;
}());
exports.default = Curriculum;
