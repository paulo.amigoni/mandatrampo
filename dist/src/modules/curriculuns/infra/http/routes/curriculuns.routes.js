"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var celebrate_1 = require("celebrate");
var ensureAuthenticated_1 = __importDefault(require("@modules/users/infra/http/middlewares/ensureAuthenticated"));
var CurriculumController_1 = __importDefault(require("../controllers/CurriculumController"));
var curriculunsRouter = express_1.Router();
var curriculumController = new CurriculumController_1.default();
curriculunsRouter.use(ensureAuthenticated_1.default);
curriculunsRouter.post('/', celebrate_1.celebrate((_a = {},
    _a[celebrate_1.Segments.BODY] = {
        user_id: celebrate_1.Joi.string().uuid().required(),
        curriculum: celebrate_1.Joi.string(),
        link_mediasocial: celebrate_1.Joi.string(),
        description: celebrate_1.Joi.string(),
        profession_id: celebrate_1.Joi.string().required(),
        profession_others: celebrate_1.Joi.string(),
        experience_time: celebrate_1.Joi.string(),
    },
    _a)), curriculumController.create);
exports.default = curriculunsRouter;
