### BackEnd Projeto mandaTrampo

:mask: Devido a pandemia estabelecida ao redor do nosso planeta, a turma de Sistemas para Internet :mortar_board:  está desenvolvendo um jeitinho para ajudar quem está precisando, nesse momento dificil ou não. :mask:





- API pra integração do Projeto MandaTrampo FATEC Araras

<p align="center">
   <img src="http://img.shields.io/static/v1?label=STATUS&message=EM%20DESENVOLVIMENTO&color=RED&style=for-the-badge"/>
</p>

### Tarefas completas ou em execução

- [x] Cadastro de usuário
- [x] Permitir acesso de usuário
- [x] Cadastro de Serviços
- [x] Cadastro de Curriculum
- [x] Cadastro de Profissão
- [x] Cadastro de Categoria
- [x] Cadastro de Imagens para Serviço
- [x] Listagem de Serviços
- [x] Listagem de Serviços por Categoria
- [x] Listagem de Serviços por Usuário
- [x] Listagem de Curriculum
- [x] Listagem de Curriculum por Profissão
- [x] Update Cadastro de Curriculum
- [x] Update Cadastro de Serviço
- [x] Update Usuário
- [ ] Envio de E-mail
- [ ] Aprovação do Serviço



## Como rodar a aplicação :arrow_forward:

No terminal, clone o projeto após clonar:

Instale as dependecias:

```
yarn install
```

Altere o aquivo .env e entre com as credênciais
Altere o ormconfig e entre com as credênciais

Execute a aplicação:

```
yarn dev:start
```

Se tudo estiver certo, vai aparecer uma mensagem que o servidor está funcionando.
Pronto, agora é possível acessar a aplicação a partir da rota http://localhost:3000/

## Rotas


| Rota            | Método       | Requer Autenticação  |
|-----------------|:-------------|:--------------------:|
| /users          | POST         |        :-1:          |

:speech_balloon: Cadastro basico para acesso ao sistema

> Enviar

```
{
"name" : "Everton",
"email": "everton@email.com",
"login": "barramansa",
"password":"123456"
}
```

> Retorno

```
{
"name": "Everton",
"email": "everton@email.com",
"id": "a470aa83-59c6-48ea-a43e-10f83189e094",
"avatar_url": null
}
```
________________________________________________________________________________



| Rota            | Método       | Requer Autenticação  |
|-----------------|:-------------|:--------------------:|
| /sessions       | POST         |        :-1:          |

:speech_balloon: Login para o sistema

> Enviar

```
{
"login": "barramansa",
"password": "123456"
}
```

> Retorno

```
{
  "user": {
    "id": "a470aa83-59c6-48ea-a43e-10f83189e094",
    "name": "Everton",
    "email": "everton@email.com",
    "avatar": null,
    "phone": null,
    "celphone": null,
    "address": null,
    "city": null,
    "country": null,
    "avatar_url": null
  },
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTI0MTQzMTksImV4cCI6MTU5MjU4NzExOSwic3ViIjoiYTQ3MGFhODMtNTljNi00OGVhLWE0M2UtMTBmODMxODllMDk0In0.3xk98hfAEsU-aBSki4kdvDQvWrE2Nb9KSHcpxZ3uhg0",
  "countService": 0,
  "serviceAproved": 0,
  "serviceReproved": 0
}
```
________________________________________________________________________________




| Rota            | Método       | Requer Autenticação  | Tipo de Autenticação |
|-----------------|:-------------|:--------------------:|:--------------------:|
| /profile        | PUT          |     :thumbsup:       |    Bearer - Token    |

:speech_balloon: Alteração do Usuário com dados faltantes

> Enviar

```
{
	"name": "EVERTON BARRAMANSA",
	"email": "evertoni@email.com",
	"phone": "55199999999999",
	"celphone": "55199999999999",
	"address" : "ANTONIO DE ALMEIDA",
	"login" : "barramansa",
	"city": "ARARAS",
	"country": "SP",
	"iswhats": true
}
```

> Retorno

```
{
  "id": "a470aa83-59c6-48ea-a43e-10f83189e094",
  "name": "EVERTON BARRAMANSA",
  "email": "evertoni@email.com",
  "avatar": null,
  "phone": "55199999999999",
  "celphone": "55199999999999",
  "address": "ANTONIO DE ALMEIDA",
  "city": "ARARAS",
  "country": "SP",
  "iswhats": true,
  "avatar_url": null
}
```
________________________________________________________________________________




| Rota            | Método       | Requer Autenticação  | Tipo de Autenticação | Requer Parametros    |
|-----------------|:-------------|:--------------------:|:--------------------:|:--------------------:|
| /profile        | PATCH        |     :thumbsup:       |    Bearer - Token    |  Multpart/form-data  |

:speech_balloon: Alteração do Avatar Usuário

> Enviar

Enviar o file como input:name ( avatar )

> Retorno

```
{
  "id": "a470aa83-59c6-48ea-a43e-10f83189e094",
  "name": "EVERTON BARRAMANSA",
  "email": "evertoni@email.com",
  "avatar": avatar.png,
  "phone": "55199999999999",
  "celphone": "55199999999999",
  "address": "ANTONIO DE ALMEIDA",
  "city": "ARARAS",
  "country": "SP",
  "iswhats": true,
  "avatar_url": "http://www.mandatrampo.com.br/avatar.png"
}
```
________________________________________________________________________________







[![Abra no Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=&uri=https%3A%2F%2Fgitlab.com%2Fmandatrampo%2Fbackend%2F-%2Fedit%2Fdevelopment%2FInsomnia.json)


## Linguagens e libs utilizadas :books:

- [NodeJs]: versão 12

## Desenvolvedores/Contribuintes

| [<img src="https://avatars1.githubusercontent.com/u/2119919?s=460&u=21b7da9ce9be2163076f1fa85f0986198f98f152&v=4" width=115 > <br> <sub> Paulo Amigoni </sub>](https://github.com/pauloamigoni) | [<img src="https://avatars1.githubusercontent.com/u/38158576?s=460&u=3234c66d4177b59c240780628dd0d7eab074ff7b&v=4" width=115 > <br> <sub> Fabio Suzarth</sub>](https://github.com/fabiosuzarth) | [<img src="https://avatars0.githubusercontent.com/u/53358155?s=460&u=822996342e5d9b7f57aeb85ce399b0e1664ab852&v=4" width=115 > <br> <sub> Jhonny Marques</sub>](https://github.com/jhonnymarques) | [<img src="https://avatars2.githubusercontent.com/u/47534815?s=460&v=4" width=115 > <br> <sub> Jair Lopes Junior </sub>](https://github.com/JairLopesJunior) | [<img src="https://avatars3.githubusercontent.com/u/51486508?s=460&u=aea8d6c7921b7a053f23bf0480d6154d747fa23d&v=4" width=115 > <br> <sub> Everton Barramansa </sub>](https://github.com/evertonbarramansa) |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |


## Licença

The [MIT License]() (MIT)

Copyright :copyright: 2020 - MandaTrampo FATEC
